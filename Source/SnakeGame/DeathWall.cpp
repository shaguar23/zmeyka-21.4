// Fill out your copyright notice in the Description page of Project Settings.


#include "DeathWall.h"
#include "SnakeBase.h"

#include "Components/BoxComponent.h"

// Sets default values
ADeathWall::ADeathWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	class UStaticMesh* WallMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("/Engine/BasicShapes/Cube")).Object;
	WallColour = ConstructorHelpers::FObjectFinderOptional<UMaterialInstance>(TEXT("MaterialInstanceConstant'/Game/Material/Instance/BasicAsset01_Inst.BasicAsset01_Inst'")).Get();

	ADeathBox = CreateDefaultSubobject<UBoxComponent>("RootModel");

	RootComponent = ADeathBox;

	class UStaticMeshComponent* WallBreak;
	WallBreak = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WALL"));
	WallBreak->SetStaticMesh(WallMesh);
	WallBreak->SetRelativeLocation(FVector(0, 0, 0));
	WallBreak->SetMaterial(0, WallColour);
	WallBreak->AttachTo(ADeathBox);
}

// Called when the game starts or when spawned
void ADeathWall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADeathWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CollideWall();

}

void ADeathWall::CollideWall()
{
	TArray<AActor*> CollectedActors;
	GetOverlappingActors(CollectedActors);

	for (int32 i = 0; i < CollectedActors.Num(); ++i)
	{
		CollectedActors[i]->Destroy(true, true);
	}
}

