// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();

			FRotator NewRotator(0, 0, 0);
			float MinX = -440.f;
			float MaxX = 440.f;
			float MinY = -440.f;
			float MaxY = 440.f;

			float RandomX = FMath::RandRange(MinX, MaxX);
			float RandomY = FMath::RandRange(MinY, MaxY);

			FVector FoodSpawnLocation(RandomX, RandomY, 0);
			GetWorld()->SpawnActor<AFood>(this->GetClass(), FoodSpawnLocation, NewRotator);

			Destroy();
		}
	}
}

